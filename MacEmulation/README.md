Mac Emulation
=============

The Amiga is the perfect machine to emulate "old world" Apple Macintosh models. Those are the ones with Motorola 680x0 processors that are capable of running MacOS up to version 8.1.

Because the processor range is the same as in the classic Amiga systems, "only" the differences between Amiga and Macintosh have to be emulated, i.e. the way graphics adapters, soundcards, SCSI controllers, etc. are accessed.
This often leads to the emulation [running faster](https://www.youtube.com/watch?v=Jph0gxzL3UI) than a real Apple system of that era.

There are different approaches to Macintosh emulation on the Amiga. All of them have in common that you need an image of an actual Macintosh ROM and a legal copy of MacOS to be able to use them.

I'll give a short overview of the three emulators I personally tried.

EMPLANT Macintosh Emulation
---------------------------

This is the oldest of the three variants. It is only useable in systems that can provide a Zorro II or III slot. It consists of a [Zorro II board called EMPLANT](http://amiga.resource.cx/exp/emplant) and accompanying software.

The EMPLANT board has been available in several versions and stages of expansion:

* A basic variant, which had the primary use of reading the contents of an original Macintosh ROM chip or module and providing the Amiga with two real [Apple Desktop Bus (ADB)](https://en.wikipedia.org/wiki/Apple_Desktop_Bus) ports with the help of two [VIA chips](https://en.wikipedia.org/wiki/MOS_Technology_6522).
* A "deluxe" variant, which in addition incorporated a SCSI-I controller (NCR 53C80) and a third VIA chip.
* And an optional upgrade to the e586 module, where a chip on the board was changed for a newer version and a extra software is provided to emulate an Intel 80586 (i.e. Pentium) machine.

While being state of the art at its time, the original EMPLANT software has several drawbacks:

* ~~It **doesn't support** Motorola **68060** CPUs.~~ Jim Drew, the original author says it does.
* ~~It only supports MacOS up to 7~~ The original author says it supports MacOS 6.x through 8.1
* It needs an **NTSC** screen for its setup. Owners of PAL machines have to have the `NTSC` driver in `DEVS:Monitors`

FUSION Macintosh Emulator
-------------------------

FUSION could be called an advanced version of the EMPLANT software and has been written by the same author. While the latter **needs** an EMPLANT card, the former does **not need** it but is able to use it if present.

FUSION is very versatile and readily runs all **versions of MacOS from 6.x to 8.1**. Jim Drew claims it can run 8.5 from universal installation media because the 68k code is still there.
I was, however, unable to find any 8.5 installation medium that doesn't complain about not being able to run on the given hardware. So, either the said 8.5 universal install is extremely rare and hard to obtain
or the statement Wikipedia (amongst other sources) provides that 8.1 is the last MacOS version to be able to run on pure Motorola 68k systems is true.

Fusion comes with a plethora of graphics drivers both for plain Amigas and ones equipped with a graphics card.
It supports **Motorola 68020 through 68060** processors and does make use of MMU and FPU if available. It supports and emulates a large range of peripherals, e.g.

* serial and parallel ports
* all sorts of **SCSI** devices supported in MacOS
* **CD-ROM** drives
* **Ethernet** interfaces (s. notes below)
* ADB interfaces
* **Sound** output and **sampling**, either directly via Paula or by means of AHI

While this all sounds very nice, FUSION also has its drawbacks:

* It's closed source and the author has no intention to change that.
* It **cannot** be obtained legally at the moment. The author says he is working on a new release that will be available someday in the future. However, he's unable to tell when this future will be.
* ~~It is **very** picky with ethernet device drivers.~~ See notes and recipe below.
* It **doesn't work reliably** in combination with Phase5 68060 accelerator boards and the [MMUlib package](http://aminet.net/package/util/libs/MMULib).
  This is because FUSION needs exclusive access to the MMU to be able to patch many bugs in MacOS and well known applications on the fly.
  With the [original Phase5 libraries](http://aminet.net/package/driver/other/68060-19101999) it runs just fine.
* The setup interface is different from what you usually see on the Amiga. Reading the documentation helps.

FUSION is my favourite because it generally runs very stable once you sorted all the CPU library stuff. If handled correctly it runs demanding applications and games very smoothly.

ShapeShifter
------------

[ShapeShifter](http://aminet.net/package/misc/emu/ShapeShifter311) is probably the most popular Mac emulator for the Amiga. While in many ways similar to FUSION, there are some more or less subtle differences:

* It's [open source](http://aminet.net/package/misc/emu/ShapeShifter_src) and thus readily obtainable from [Aminet](http://aminet.net)
* It is **abandonware** as well as the AmigaOS port of its successor [Basilisk II](https://basilisk.cebix.net/), which I couldn't get to work on my Amiga 4000T.
* It is **picky** about the ROM files it accepts and does all kinds of crazy stuff when the ROM isn't the right one. From non-boot to crashes to an endless loop of opening and closing [Finder](https://en.wikipedia.org/wiki/Finder_(software)) everything is possible.
* I **couldn't** convince my copy to run MacOS 8.1 so far. MacOS 8.0 runs fine.
* You'll probably have to find a good **external video driver** for your hard- and software setup as the original drivers are rather slow.
* It doesn't provide much control from the Amiga side while the emulation is running.
* It **works** perfectly **fine with ethernet** drivers that adhere to the **SANA-II specification**. Such drivers can be used from the Mac and the Amiga side simultaneously on the same network hardware.

Recipes
-------

### Ethernet workaround for FUSION and ShapeShifter for unsupported drivers

Source: [English Amiga Board Thread](http://eab.abime.net/showthread.php?t=102321)

If you provided the driver for an Amiga ethernet device in the emulation settings but [OpenTransport](https://en.wikipedia.org/wiki/Open_Transport) inside the emulated Mac claims that 'Ethernet' is no longer available, the device driver isn't compatible.
This is more often the case in FUSION than in ShapeShifter, but it happens in both emulators.

The reason is that the Mac uses the same data structures for Ethernet access as the Amiga does but seems to be a bit more "picky" than most Amiga TCP/IP stacks. Generally speaking, both FUSION and Shapeshifter provide a minimal set of routines to open the device, claim ownership,
etc. and then just shovel the data structures back and forth between the Amiga's driver and the MacOS TCP/IP stack.

Luckily, there's a solution for drivers that give the above mentioned problems in MacOS. It needs the [NullSana](http://aminet.net/package/comm/tcp/nullsana) device driver and the [Roadshow](http://roadshow.apc-tcp.de/index-de.php) TCP/IP stack.

This guide assumes you have Roadshow with internet access running successfully on your ethernet card with the **interface name** `Enc624J6Net` in the subnet `192.168.1.0/24` with IP address `192.168.1.42` on your Amiga.

* Unpack the nullsana LHA archive, place `nullsana.device` in `DEVS:Networks` and `nullsana0.config` as well as `nullsana1.config` in the `ENVARC:sana2` directory.
* Create a file `DEVS:NetInterfaces/NullSana` with the following contents:
```
device=nullsana.device
unit=1
address=192.168.0.1
netmask=255.255.255.0
requiresinitdelay=no
```
* Enable **IP forwarding** in RoadShow `SetEnv SAVE Roadshow/ip/forwarding 1`
* Add the following line to `DEVS:Internet/routes`
```
NETDESTINATION 192.168.1.0 VIA 192.168.0.1
```
* Change the file `S:IPF/ipnat.rules` to read:
```
map Enc624J6Net 192.168.0.0/24 -> Enc624J6Net/32 proxy port ftp ftp/tcp
map Enc624J6Net 192.168.0.0/24 -> Enc624J6Net/32 portmap tcp/udp 10000:20000
map Enc624J6Net 192.168.0.0/24 -> Enc624J6Net/32
```
* Add the line `S:Start-Firewall` to the bottom of `S:User-Startup`
* Reboot the Amiga
* Start the configuration interface of FUSION or ShapeShifter and put `nullsana.device`, unit `0` in the configuration fields for the ethernet interface.
* Start the emulation. In MacOS open *Control Panels* -> *TCP/IP*. Choose *Ethernet* as the connection type and configure as follows:
  * IP-Address: `192.168.0.2`
  * Netmask: `255.255.255.0`
  * Gateway: `192.168.0.1`
  * DNS Server: *The same one you use in your LAN*
  * Domains: *DNS domains you want to use transparently*
* Save the settings
* **Congratulations!** Your emulated Macintosh is now online.

Special thanks to user **funK** from [English Amiga Board](http://eab.abime.net) and to Jim Drew for straightening some things up about EMPLANT and FUSION.
